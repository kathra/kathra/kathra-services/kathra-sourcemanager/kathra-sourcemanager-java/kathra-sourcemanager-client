/*
 * Copyright (c) 2020. The Kathra Authors.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 *    IRT SystemX (https://www.kathra.org/)
 *
 */
package org.kathra.sourcemanager.client;

import org.kathra.client.*;
import java.util.ArrayList;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;
import org.kathra.utils.KathraSessionManager;
import org.kathra.utils.ApiException;
import org.kathra.utils.ApiResponse;

import java.io.File;
import org.kathra.sourcemanager.model.Folder;
import java.util.List;
import org.kathra.core.model.Membership;
import org.kathra.core.model.SourceRepository;
import org.kathra.core.model.SourceRepositoryCommit;

public class SourceManagerClient {
    private ApiClient apiClient;

    public SourceManagerClient() {
        this.apiClient = new ApiClient().setUserAgent("SourceManagerClient 1.0.0");
    }

    public SourceManagerClient(String serviceUrl) {
        this();
        apiClient.setBasePath(serviceUrl);
    }

    public SourceManagerClient(String serviceUrl, KathraSessionManager sessionManager) {
        this(serviceUrl);
        apiClient.setSessionManager(sessionManager);
    }

    public ApiClient getApiClient() {
        return apiClient;
    }

    private void setApiClient(ApiClient apiClient) {
        this.apiClient = apiClient;
    }

    /**
     * Build call for addMemberships
     * @param memberships  (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call addMembershipsCall(List<Membership> memberships, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = memberships;
        
        // create path and map variables
        String localVarPath = "/batch/memberships";

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call addMembershipsValidateBeforeCall(List<Membership> memberships, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'memberships' is set
        if (memberships == null) {
            throw new ApiException("Missing the required parameter 'memberships' when calling addMemberships(Async)");
        }
        
        
        com.squareup.okhttp.Call call = addMembershipsCall(memberships, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Add multiple memberships in specified projects
     * 
     * @param memberships  (required)
     * @return ApiResponse
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse addMemberships(List<Membership> memberships) throws ApiException {
        ApiResponse<ApiResponse> resp = addMembershipsWithHttpInfo(memberships);
        return resp.getData();
    }

    /**
     * Add multiple memberships in specified projects
     * 
     * @param memberships  (required)
     * @return ApiResponse&lt;ApiResponse&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<ApiResponse> addMembershipsWithHttpInfo(List<Membership> memberships) throws ApiException {
        com.squareup.okhttp.Call call = addMembershipsValidateBeforeCall(memberships, null, null);
        Type localVarReturnType = new TypeToken<ApiResponse>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Add multiple memberships in specified projects (asynchronously)
     * 
     * @param memberships  (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call addMembershipsAsync(List<Membership> memberships, final ApiCallback<ApiResponse> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = addMembershipsValidateBeforeCall(memberships, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<ApiResponse>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for createBranch
     * @param sourceRepositoryPath SourceRepository's Path (required)
     * @param branch  (required)
     * @param branchRef  (optional)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call createBranchCall(String sourceRepositoryPath, String branch, String branchRef, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = branch;
        
        // create path and map variables
        String localVarPath = "/sourceRepositories/{sourceRepositoryPath}/branches"
            .replaceAll("\\{" + "sourceRepositoryPath" + "\\}", apiClient.escapeString(sourceRepositoryPath.toString()));

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();
        if (branchRef != null)
        localVarQueryParams.addAll(apiClient.parameterToPair("branchRef", branchRef));

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call createBranchValidateBeforeCall(String sourceRepositoryPath, String branch, String branchRef, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'sourceRepositoryPath' is set
        if (sourceRepositoryPath == null) {
            throw new ApiException("Missing the required parameter 'sourceRepositoryPath' when calling createBranch(Async)");
        }
        
        // verify the required parameter 'branch' is set
        if (branch == null) {
            throw new ApiException("Missing the required parameter 'branch' when calling createBranch(Async)");
        }
        
        
        com.squareup.okhttp.Call call = createBranchCall(sourceRepositoryPath, branch, branchRef, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Create a new branch in an existing Kathra SourceRepository
     * 
     * @param sourceRepositoryPath SourceRepository's Path (required)
     * @param branch  (required)
     * @param branchRef  (optional)
     * @return String
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public String createBranch(String sourceRepositoryPath, String branch, String branchRef) throws ApiException {
        ApiResponse<String> resp = createBranchWithHttpInfo(sourceRepositoryPath, branch, branchRef);
        return resp.getData();
    }

    /**
     * Create a new branch in an existing Kathra SourceRepository
     * 
     * @param sourceRepositoryPath SourceRepository's Path (required)
     * @param branch  (required)
     * @param branchRef  (optional)
     * @return ApiResponse&lt;String&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<String> createBranchWithHttpInfo(String sourceRepositoryPath, String branch, String branchRef) throws ApiException {
        com.squareup.okhttp.Call call = createBranchValidateBeforeCall(sourceRepositoryPath, branch, branchRef, null, null);
        Type localVarReturnType = new TypeToken<String>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Create a new branch in an existing Kathra SourceRepository (asynchronously)
     * 
     * @param sourceRepositoryPath SourceRepository's Path (required)
     * @param branch  (required)
     * @param branchRef  (optional)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call createBranchAsync(String sourceRepositoryPath, String branch, String branchRef, final ApiCallback<String> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = createBranchValidateBeforeCall(sourceRepositoryPath, branch, branchRef, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<String>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for createCommit
     * @param sourceRepositoryPath SourceRepository's Path (required)
     * @param branch SourceRepository's branch (required)
     * @param file File to commit (required)
     * @param filepath The location in which the file has to be commited (optional)
     * @param uncompress Boolean to indicate if provided file should be uncompressed before being commited (optional, default to false)
     * @param tag Git tag to put on the commit (optional)
     * @param replaceRepositoryContent Boolean to indicate if provided file should replace the whole repository content (optional, default to false)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call createCommitCall(String sourceRepositoryPath, String branch, File file, String filepath, Boolean uncompress, String tag, Boolean replaceRepositoryContent, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/sourceRepositories/{sourceRepositoryPath}/branches/{branch}/commits"
            .replaceAll("\\{" + "sourceRepositoryPath" + "\\}", apiClient.escapeString(sourceRepositoryPath.toString()))
            .replaceAll("\\{" + "branch" + "\\}", apiClient.escapeString(branch.toString()));

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();
        if (filepath != null)
        localVarQueryParams.addAll(apiClient.parameterToPair("filepath", filepath));
        if (uncompress != null)
        localVarQueryParams.addAll(apiClient.parameterToPair("uncompress", uncompress));
        if (tag != null)
        localVarQueryParams.addAll(apiClient.parameterToPair("tag", tag));
        if (replaceRepositoryContent != null)
        localVarQueryParams.addAll(apiClient.parameterToPair("replaceRepositoryContent", replaceRepositoryContent));

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();
        if (file != null)
        localVarFormParams.put("file", file);

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "multipart/form-data"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call createCommitValidateBeforeCall(String sourceRepositoryPath, String branch, File file, String filepath, Boolean uncompress, String tag, Boolean replaceRepositoryContent, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'sourceRepositoryPath' is set
        if (sourceRepositoryPath == null) {
            throw new ApiException("Missing the required parameter 'sourceRepositoryPath' when calling createCommit(Async)");
        }
        
        // verify the required parameter 'branch' is set
        if (branch == null) {
            throw new ApiException("Missing the required parameter 'branch' when calling createCommit(Async)");
        }
        
        // verify the required parameter 'file' is set
        if (file == null) {
            throw new ApiException("Missing the required parameter 'file' when calling createCommit(Async)");
        }
        
        
        com.squareup.okhttp.Call call = createCommitCall(sourceRepositoryPath, branch, file, filepath, uncompress, tag, replaceRepositoryContent, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Create new commit in branch
     * 
     * @param sourceRepositoryPath SourceRepository's Path (required)
     * @param branch SourceRepository's branch (required)
     * @param file File to commit (required)
     * @param filepath The location in which the file has to be commited (optional)
     * @param uncompress Boolean to indicate if provided file should be uncompressed before being commited (optional, default to false)
     * @param tag Git tag to put on the commit (optional)
     * @param replaceRepositoryContent Boolean to indicate if provided file should replace the whole repository content (optional, default to false)
     * @return SourceRepositoryCommit
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public SourceRepositoryCommit createCommit(String sourceRepositoryPath, String branch, File file, String filepath, Boolean uncompress, String tag, Boolean replaceRepositoryContent) throws ApiException {
        ApiResponse<SourceRepositoryCommit> resp = createCommitWithHttpInfo(sourceRepositoryPath, branch, file, filepath, uncompress, tag, replaceRepositoryContent);
        return resp.getData();
    }

    /**
     * Create new commit in branch
     * 
     * @param sourceRepositoryPath SourceRepository's Path (required)
     * @param branch SourceRepository's branch (required)
     * @param file File to commit (required)
     * @param filepath The location in which the file has to be commited (optional)
     * @param uncompress Boolean to indicate if provided file should be uncompressed before being commited (optional, default to false)
     * @param tag Git tag to put on the commit (optional)
     * @param replaceRepositoryContent Boolean to indicate if provided file should replace the whole repository content (optional, default to false)
     * @return ApiResponse&lt;SourceRepositoryCommit&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<SourceRepositoryCommit> createCommitWithHttpInfo(String sourceRepositoryPath, String branch, File file, String filepath, Boolean uncompress, String tag, Boolean replaceRepositoryContent) throws ApiException {
        com.squareup.okhttp.Call call = createCommitValidateBeforeCall(sourceRepositoryPath, branch, file, filepath, uncompress, tag, replaceRepositoryContent, null, null);
        Type localVarReturnType = new TypeToken<SourceRepositoryCommit>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Create new commit in branch (asynchronously)
     * 
     * @param sourceRepositoryPath SourceRepository's Path (required)
     * @param branch SourceRepository's branch (required)
     * @param file File to commit (required)
     * @param filepath The location in which the file has to be commited (optional)
     * @param uncompress Boolean to indicate if provided file should be uncompressed before being commited (optional, default to false)
     * @param tag Git tag to put on the commit (optional)
     * @param replaceRepositoryContent Boolean to indicate if provided file should replace the whole repository content (optional, default to false)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call createCommitAsync(String sourceRepositoryPath, String branch, File file, String filepath, Boolean uncompress, String tag, Boolean replaceRepositoryContent, final ApiCallback<SourceRepositoryCommit> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = createCommitValidateBeforeCall(sourceRepositoryPath, branch, file, filepath, uncompress, tag, replaceRepositoryContent, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<SourceRepositoryCommit>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for createDeployKey
     * @param keyName  (required)
     * @param sshPublicKey  (required)
     * @param sourceRepositoryPath SourceRepository's Path (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call createDeployKeyCall(String keyName, String sshPublicKey, String sourceRepositoryPath, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = sshPublicKey;
        
        // create path and map variables
        String localVarPath = "/sourceRepositories/{sourceRepositoryPath}/deployKeys"
            .replaceAll("\\{" + "sourceRepositoryPath" + "\\}", apiClient.escapeString(sourceRepositoryPath.toString()));

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();
        if (keyName != null)
        localVarQueryParams.addAll(apiClient.parameterToPair("keyName", keyName));

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call createDeployKeyValidateBeforeCall(String keyName, String sshPublicKey, String sourceRepositoryPath, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'keyName' is set
        if (keyName == null) {
            throw new ApiException("Missing the required parameter 'keyName' when calling createDeployKey(Async)");
        }
        
        // verify the required parameter 'sshPublicKey' is set
        if (sshPublicKey == null) {
            throw new ApiException("Missing the required parameter 'sshPublicKey' when calling createDeployKey(Async)");
        }
        
        // verify the required parameter 'sourceRepositoryPath' is set
        if (sourceRepositoryPath == null) {
            throw new ApiException("Missing the required parameter 'sourceRepositoryPath' when calling createDeployKey(Async)");
        }
        
        
        com.squareup.okhttp.Call call = createDeployKeyCall(keyName, sshPublicKey, sourceRepositoryPath, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Create new deployKey in specified project
     * 
     * @param keyName  (required)
     * @param sshPublicKey  (required)
     * @param sourceRepositoryPath SourceRepository's Path (required)
     * @return ApiResponse
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse createDeployKey(String keyName, String sshPublicKey, String sourceRepositoryPath) throws ApiException {
        ApiResponse<ApiResponse> resp = createDeployKeyWithHttpInfo(keyName, sshPublicKey, sourceRepositoryPath);
        return resp.getData();
    }

    /**
     * Create new deployKey in specified project
     * 
     * @param keyName  (required)
     * @param sshPublicKey  (required)
     * @param sourceRepositoryPath SourceRepository's Path (required)
     * @return ApiResponse&lt;ApiResponse&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<ApiResponse> createDeployKeyWithHttpInfo(String keyName, String sshPublicKey, String sourceRepositoryPath) throws ApiException {
        com.squareup.okhttp.Call call = createDeployKeyValidateBeforeCall(keyName, sshPublicKey, sourceRepositoryPath, null, null);
        Type localVarReturnType = new TypeToken<ApiResponse>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Create new deployKey in specified project (asynchronously)
     * 
     * @param keyName  (required)
     * @param sshPublicKey  (required)
     * @param sourceRepositoryPath SourceRepository's Path (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call createDeployKeyAsync(String keyName, String sshPublicKey, String sourceRepositoryPath, final ApiCallback<ApiResponse> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = createDeployKeyValidateBeforeCall(keyName, sshPublicKey, sourceRepositoryPath, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<ApiResponse>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for createFolder
     * @param folder Folder object (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call createFolderCall(Folder folder, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = folder;
        
        // create path and map variables
        String localVarPath = "/folders";

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call createFolderValidateBeforeCall(Folder folder, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'folder' is set
        if (folder == null) {
            throw new ApiException("Missing the required parameter 'folder' when calling createFolder(Async)");
        }
        
        
        com.squareup.okhttp.Call call = createFolderCall(folder, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Create a new folder in the Source Repository Provider
     * 
     * @param folder Folder object (required)
     * @return Folder
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public Folder createFolder(Folder folder) throws ApiException {
        ApiResponse<Folder> resp = createFolderWithHttpInfo(folder);
        return resp.getData();
    }

    /**
     * Create a new folder in the Source Repository Provider
     * 
     * @param folder Folder object (required)
     * @return ApiResponse&lt;Folder&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<Folder> createFolderWithHttpInfo(Folder folder) throws ApiException {
        com.squareup.okhttp.Call call = createFolderValidateBeforeCall(folder, null, null);
        Type localVarReturnType = new TypeToken<Folder>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Create a new folder in the Source Repository Provider (asynchronously)
     * 
     * @param folder Folder object (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call createFolderAsync(Folder folder, final ApiCallback<Folder> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = createFolderValidateBeforeCall(folder, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<Folder>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for createSourceRepository
     * @param sourceRepository SourceRepository object to be created (required)
     * @param deployKeys A list of deployKey Ids to enable in the created source repository (optional)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call createSourceRepositoryCall(SourceRepository sourceRepository, List<String> deployKeys, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = sourceRepository;
        
        // create path and map variables
        String localVarPath = "/sourceRepositories";

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();
        if (deployKeys != null)
        localVarCollectionQueryParams.addAll(apiClient.parameterToPairs("csv", "deployKeys", deployKeys));

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call createSourceRepositoryValidateBeforeCall(SourceRepository sourceRepository, List<String> deployKeys, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'sourceRepository' is set
        if (sourceRepository == null) {
            throw new ApiException("Missing the required parameter 'sourceRepository' when calling createSourceRepository(Async)");
        }
        
        
        com.squareup.okhttp.Call call = createSourceRepositoryCall(sourceRepository, deployKeys, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Create a new Source Repository in Kathra Repository Provider
     * 
     * @param sourceRepository SourceRepository object to be created (required)
     * @param deployKeys A list of deployKey Ids to enable in the created source repository (optional)
     * @return SourceRepository
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public SourceRepository createSourceRepository(SourceRepository sourceRepository, List<String> deployKeys) throws ApiException {
        ApiResponse<SourceRepository> resp = createSourceRepositoryWithHttpInfo(sourceRepository, deployKeys);
        return resp.getData();
    }

    /**
     * Create a new Source Repository in Kathra Repository Provider
     * 
     * @param sourceRepository SourceRepository object to be created (required)
     * @param deployKeys A list of deployKey Ids to enable in the created source repository (optional)
     * @return ApiResponse&lt;SourceRepository&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<SourceRepository> createSourceRepositoryWithHttpInfo(SourceRepository sourceRepository, List<String> deployKeys) throws ApiException {
        com.squareup.okhttp.Call call = createSourceRepositoryValidateBeforeCall(sourceRepository, deployKeys, null, null);
        Type localVarReturnType = new TypeToken<SourceRepository>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Create a new Source Repository in Kathra Repository Provider (asynchronously)
     * 
     * @param sourceRepository SourceRepository object to be created (required)
     * @param deployKeys A list of deployKey Ids to enable in the created source repository (optional)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call createSourceRepositoryAsync(SourceRepository sourceRepository, List<String> deployKeys, final ApiCallback<SourceRepository> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = createSourceRepositoryValidateBeforeCall(sourceRepository, deployKeys, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<SourceRepository>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for deleteMemberships
     * @param memberships  (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call deleteMembershipsCall(List<Membership> memberships, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = memberships;
        
        // create path and map variables
        String localVarPath = "/batch/memberships";

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "DELETE", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call deleteMembershipsValidateBeforeCall(List<Membership> memberships, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'memberships' is set
        if (memberships == null) {
            throw new ApiException("Missing the required parameter 'memberships' when calling deleteMemberships(Async)");
        }
        
        
        com.squareup.okhttp.Call call = deleteMembershipsCall(memberships, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Delete multiple memberships in specified projects
     * 
     * @param memberships  (required)
     * @return ApiResponse
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse deleteMemberships(List<Membership> memberships) throws ApiException {
        ApiResponse<ApiResponse> resp = deleteMembershipsWithHttpInfo(memberships);
        return resp.getData();
    }

    /**
     * Delete multiple memberships in specified projects
     * 
     * @param memberships  (required)
     * @return ApiResponse&lt;ApiResponse&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<ApiResponse> deleteMembershipsWithHttpInfo(List<Membership> memberships) throws ApiException {
        com.squareup.okhttp.Call call = deleteMembershipsValidateBeforeCall(memberships, null, null);
        Type localVarReturnType = new TypeToken<ApiResponse>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Delete multiple memberships in specified projects (asynchronously)
     * 
     * @param memberships  (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call deleteMembershipsAsync(List<Membership> memberships, final ApiCallback<ApiResponse> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = deleteMembershipsValidateBeforeCall(memberships, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<ApiResponse>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for deleteSourceRepository
     * @param sourceRepositoryPath SourceRepository's Path (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call deleteSourceRepositoryCall(String sourceRepositoryPath, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/sourceRepositories/{sourceRepositoryPath}"
            .replaceAll("\\{" + "sourceRepositoryPath" + "\\}", apiClient.escapeString(sourceRepositoryPath.toString()));

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "DELETE", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call deleteSourceRepositoryValidateBeforeCall(String sourceRepositoryPath, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'sourceRepositoryPath' is set
        if (sourceRepositoryPath == null) {
            throw new ApiException("Missing the required parameter 'sourceRepositoryPath' when calling deleteSourceRepository(Async)");
        }
        
        
        com.squareup.okhttp.Call call = deleteSourceRepositoryCall(sourceRepositoryPath, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Delete Source Repository in Kathra Repository Provider
     * 
     * @param sourceRepositoryPath SourceRepository's Path (required)
     * @return String
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public String deleteSourceRepository(String sourceRepositoryPath) throws ApiException {
        ApiResponse<String> resp = deleteSourceRepositoryWithHttpInfo(sourceRepositoryPath);
        return resp.getData();
    }

    /**
     * Delete Source Repository in Kathra Repository Provider
     * 
     * @param sourceRepositoryPath SourceRepository's Path (required)
     * @return ApiResponse&lt;String&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<String> deleteSourceRepositoryWithHttpInfo(String sourceRepositoryPath) throws ApiException {
        com.squareup.okhttp.Call call = deleteSourceRepositoryValidateBeforeCall(sourceRepositoryPath, null, null);
        Type localVarReturnType = new TypeToken<String>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Delete Source Repository in Kathra Repository Provider (asynchronously)
     * 
     * @param sourceRepositoryPath SourceRepository's Path (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call deleteSourceRepositoryAsync(String sourceRepositoryPath, final ApiCallback<String> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = deleteSourceRepositoryValidateBeforeCall(sourceRepositoryPath, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<String>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for getBranches
     * @param sourceRepositoryPath SourceRepository's Path (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call getBranchesCall(String sourceRepositoryPath, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/sourceRepositories/{sourceRepositoryPath}/branches"
            .replaceAll("\\{" + "sourceRepositoryPath" + "\\}", apiClient.escapeString(sourceRepositoryPath.toString()));

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call getBranchesValidateBeforeCall(String sourceRepositoryPath, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'sourceRepositoryPath' is set
        if (sourceRepositoryPath == null) {
            throw new ApiException("Missing the required parameter 'sourceRepositoryPath' when calling getBranches(Async)");
        }
        
        
        com.squareup.okhttp.Call call = getBranchesCall(sourceRepositoryPath, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Retrieve accessible branches in an existing Kathra SourceRepository
     * 
     * @param sourceRepositoryPath SourceRepository's Path (required)
     * @return List&lt;String&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public List<String> getBranches(String sourceRepositoryPath) throws ApiException {
        ApiResponse<List<String>> resp = getBranchesWithHttpInfo(sourceRepositoryPath);
        return resp.getData();
    }

    /**
     * Retrieve accessible branches in an existing Kathra SourceRepository
     * 
     * @param sourceRepositoryPath SourceRepository's Path (required)
     * @return ApiResponse&lt;List&lt;String&gt;&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<List<String>> getBranchesWithHttpInfo(String sourceRepositoryPath) throws ApiException {
        com.squareup.okhttp.Call call = getBranchesValidateBeforeCall(sourceRepositoryPath, null, null);
        Type localVarReturnType = new TypeToken<List<String>>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Retrieve accessible branches in an existing Kathra SourceRepository (asynchronously)
     * 
     * @param sourceRepositoryPath SourceRepository's Path (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call getBranchesAsync(String sourceRepositoryPath, final ApiCallback<List<String>> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = getBranchesValidateBeforeCall(sourceRepositoryPath, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<List<String>>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for getCommits
     * @param sourceRepositoryPath SourceRepository's Path (required)
     * @param branch SourceRepository's branch (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call getCommitsCall(String sourceRepositoryPath, String branch, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/sourceRepositories/{sourceRepositoryPath}/branches/{branch}/commits"
            .replaceAll("\\{" + "sourceRepositoryPath" + "\\}", apiClient.escapeString(sourceRepositoryPath.toString()))
            .replaceAll("\\{" + "branch" + "\\}", apiClient.escapeString(branch.toString()));

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call getCommitsValidateBeforeCall(String sourceRepositoryPath, String branch, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'sourceRepositoryPath' is set
        if (sourceRepositoryPath == null) {
            throw new ApiException("Missing the required parameter 'sourceRepositoryPath' when calling getCommits(Async)");
        }
        
        // verify the required parameter 'branch' is set
        if (branch == null) {
            throw new ApiException("Missing the required parameter 'branch' when calling getCommits(Async)");
        }
        
        
        com.squareup.okhttp.Call call = getCommitsCall(sourceRepositoryPath, branch, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Retrieve accessible commits in an existing branch
     * 
     * @param sourceRepositoryPath SourceRepository's Path (required)
     * @param branch SourceRepository's branch (required)
     * @return List&lt;SourceRepositoryCommit&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public List<SourceRepositoryCommit> getCommits(String sourceRepositoryPath, String branch) throws ApiException {
        ApiResponse<List<SourceRepositoryCommit>> resp = getCommitsWithHttpInfo(sourceRepositoryPath, branch);
        return resp.getData();
    }

    /**
     * Retrieve accessible commits in an existing branch
     * 
     * @param sourceRepositoryPath SourceRepository's Path (required)
     * @param branch SourceRepository's branch (required)
     * @return ApiResponse&lt;List&lt;SourceRepositoryCommit&gt;&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<List<SourceRepositoryCommit>> getCommitsWithHttpInfo(String sourceRepositoryPath, String branch) throws ApiException {
        com.squareup.okhttp.Call call = getCommitsValidateBeforeCall(sourceRepositoryPath, branch, null, null);
        Type localVarReturnType = new TypeToken<List<SourceRepositoryCommit>>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Retrieve accessible commits in an existing branch (asynchronously)
     * 
     * @param sourceRepositoryPath SourceRepository's Path (required)
     * @param branch SourceRepository's branch (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call getCommitsAsync(String sourceRepositoryPath, String branch, final ApiCallback<List<SourceRepositoryCommit>> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = getCommitsValidateBeforeCall(sourceRepositoryPath, branch, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<List<SourceRepositoryCommit>>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for getFile
     * @param sourceRepositoryPath SourceRepository's Path (required)
     * @param branch SourceRepository's branch (required)
     * @param filepath The location of the file to retrieve (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call getFileCall(String sourceRepositoryPath, String branch, String filepath, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/sourceRepositories/{sourceRepositoryPath}/branches/{branch}/file"
            .replaceAll("\\{" + "sourceRepositoryPath" + "\\}", apiClient.escapeString(sourceRepositoryPath.toString()))
            .replaceAll("\\{" + "branch" + "\\}", apiClient.escapeString(branch.toString()));

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();
        if (filepath != null)
        localVarQueryParams.addAll(apiClient.parameterToPair("filepath", filepath));

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call getFileValidateBeforeCall(String sourceRepositoryPath, String branch, String filepath, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'sourceRepositoryPath' is set
        if (sourceRepositoryPath == null) {
            throw new ApiException("Missing the required parameter 'sourceRepositoryPath' when calling getFile(Async)");
        }
        
        // verify the required parameter 'branch' is set
        if (branch == null) {
            throw new ApiException("Missing the required parameter 'branch' when calling getFile(Async)");
        }
        
        // verify the required parameter 'filepath' is set
        if (filepath == null) {
            throw new ApiException("Missing the required parameter 'filepath' when calling getFile(Async)");
        }
        
        
        com.squareup.okhttp.Call call = getFileCall(sourceRepositoryPath, branch, filepath, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Retrieve an existing file in an existing branch
     * 
     * @param sourceRepositoryPath SourceRepository's Path (required)
     * @param branch SourceRepository's branch (required)
     * @param filepath The location of the file to retrieve (required)
     * @return File
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public File getFile(String sourceRepositoryPath, String branch, String filepath) throws ApiException {
        ApiResponse<File> resp = getFileWithHttpInfo(sourceRepositoryPath, branch, filepath);
        return resp.getData();
    }

    /**
     * Retrieve an existing file in an existing branch
     * 
     * @param sourceRepositoryPath SourceRepository's Path (required)
     * @param branch SourceRepository's branch (required)
     * @param filepath The location of the file to retrieve (required)
     * @return ApiResponse&lt;File&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<File> getFileWithHttpInfo(String sourceRepositoryPath, String branch, String filepath) throws ApiException {
        com.squareup.okhttp.Call call = getFileValidateBeforeCall(sourceRepositoryPath, branch, filepath, null, null);
        Type localVarReturnType = new TypeToken<File>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Retrieve an existing file in an existing branch (asynchronously)
     * 
     * @param sourceRepositoryPath SourceRepository's Path (required)
     * @param branch SourceRepository's branch (required)
     * @param filepath The location of the file to retrieve (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call getFileAsync(String sourceRepositoryPath, String branch, String filepath, final ApiCallback<File> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = getFileValidateBeforeCall(sourceRepositoryPath, branch, filepath, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<File>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for getFolder
     * @param folderPath Folder's ID in which artifacts will be created (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call getFolderCall(String folderPath, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/folders/{folderPath}"
            .replaceAll("\\{" + "folderPath" + "\\}", apiClient.escapeString(folderPath.toString()));

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call getFolderValidateBeforeCall(String folderPath, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'folderPath' is set
        if (folderPath == null) {
            throw new ApiException("Missing the required parameter 'folderPath' when calling getFolder(Async)");
        }
        
        
        com.squareup.okhttp.Call call = getFolderCall(folderPath, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Retrieve specified folder object
     * 
     * @param folderPath Folder's ID in which artifacts will be created (required)
     * @return Folder
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public Folder getFolder(String folderPath) throws ApiException {
        ApiResponse<Folder> resp = getFolderWithHttpInfo(folderPath);
        return resp.getData();
    }

    /**
     * Retrieve specified folder object
     * 
     * @param folderPath Folder's ID in which artifacts will be created (required)
     * @return ApiResponse&lt;Folder&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<Folder> getFolderWithHttpInfo(String folderPath) throws ApiException {
        com.squareup.okhttp.Call call = getFolderValidateBeforeCall(folderPath, null, null);
        Type localVarReturnType = new TypeToken<Folder>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Retrieve specified folder object (asynchronously)
     * 
     * @param folderPath Folder's ID in which artifacts will be created (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call getFolderAsync(String folderPath, final ApiCallback<Folder> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = getFolderValidateBeforeCall(folderPath, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<Folder>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for getFolders
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call getFoldersCall(final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/folders";

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call getFoldersValidateBeforeCall(final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        
        com.squareup.okhttp.Call call = getFoldersCall(progressListener, progressRequestListener);
        return call;

    }

    /**
     * Retrieve accessible folders for user using provided identity
     * 
     * @return List&lt;Folder&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public List<Folder> getFolders() throws ApiException {
        ApiResponse<List<Folder>> resp = getFoldersWithHttpInfo();
        return resp.getData();
    }

    /**
     * Retrieve accessible folders for user using provided identity
     * 
     * @return ApiResponse&lt;List&lt;Folder&gt;&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<List<Folder>> getFoldersWithHttpInfo() throws ApiException {
        com.squareup.okhttp.Call call = getFoldersValidateBeforeCall(null, null);
        Type localVarReturnType = new TypeToken<List<Folder>>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Retrieve accessible folders for user using provided identity (asynchronously)
     * 
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call getFoldersAsync(final ApiCallback<List<Folder>> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = getFoldersValidateBeforeCall(progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<List<Folder>>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for getMemberships
     * @param sourceRepositoryPath SourceRepository's Path (required)
     * @param memberType Type of memberships to retrieve, return all types if not specified (optional)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call getMembershipsCall(String sourceRepositoryPath, String memberType, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/sourceRepositories/{sourceRepositoryPath}/memberships"
            .replaceAll("\\{" + "sourceRepositoryPath" + "\\}", apiClient.escapeString(sourceRepositoryPath.toString()));

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();
        if (memberType != null)
        localVarQueryParams.addAll(apiClient.parameterToPair("memberType", memberType));

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "multipart/form-data"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call getMembershipsValidateBeforeCall(String sourceRepositoryPath, String memberType, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'sourceRepositoryPath' is set
        if (sourceRepositoryPath == null) {
            throw new ApiException("Missing the required parameter 'sourceRepositoryPath' when calling getMemberships(Async)");
        }
        
        
        com.squareup.okhttp.Call call = getMembershipsCall(sourceRepositoryPath, memberType, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Retrieve memberships in specified project
     * 
     * @param sourceRepositoryPath SourceRepository's Path (required)
     * @param memberType Type of memberships to retrieve, return all types if not specified (optional)
     * @return List&lt;Membership&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public List<Membership> getMemberships(String sourceRepositoryPath, String memberType) throws ApiException {
        ApiResponse<List<Membership>> resp = getMembershipsWithHttpInfo(sourceRepositoryPath, memberType);
        return resp.getData();
    }

    /**
     * Retrieve memberships in specified project
     * 
     * @param sourceRepositoryPath SourceRepository's Path (required)
     * @param memberType Type of memberships to retrieve, return all types if not specified (optional)
     * @return ApiResponse&lt;List&lt;Membership&gt;&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<List<Membership>> getMembershipsWithHttpInfo(String sourceRepositoryPath, String memberType) throws ApiException {
        com.squareup.okhttp.Call call = getMembershipsValidateBeforeCall(sourceRepositoryPath, memberType, null, null);
        Type localVarReturnType = new TypeToken<List<Membership>>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Retrieve memberships in specified project (asynchronously)
     * 
     * @param sourceRepositoryPath SourceRepository's Path (required)
     * @param memberType Type of memberships to retrieve, return all types if not specified (optional)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call getMembershipsAsync(String sourceRepositoryPath, String memberType, final ApiCallback<List<Membership>> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = getMembershipsValidateBeforeCall(sourceRepositoryPath, memberType, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<List<Membership>>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
    /**
     * Build call for getSourceRepositoriesInFolder
     * @param folderPath Folder's ID in which artifacts will be created (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call getSourceRepositoriesInFolderCall(String folderPath, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;
        
        // create path and map variables
        String localVarPath = "/folders/{folderPath}/sourceRepositories"
            .replaceAll("\\{" + "folderPath" + "\\}", apiClient.escapeString(folderPath.toString()));

        List<Pair> localVarQueryParams = new ArrayList();
        List<Pair> localVarCollectionQueryParams = new ArrayList();

        Map<String, String> localVarHeaderParams = new HashMap();

        Map<String, Object> localVarFormParams = new HashMap();

        final String[] localVarAccepts = {
            "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
            "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(chain -> {
                com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                return originalResponse.newBuilder()
                .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                .build();
            });
        }

        String[] localVarAuthNames = new String[] { "kathra_auth" };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarCollectionQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }
    
    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call getSourceRepositoriesInFolderValidateBeforeCall(String folderPath, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        
        // verify the required parameter 'folderPath' is set
        if (folderPath == null) {
            throw new ApiException("Missing the required parameter 'folderPath' when calling getSourceRepositoriesInFolder(Async)");
        }
        
        
        com.squareup.okhttp.Call call = getSourceRepositoriesInFolderCall(folderPath, progressListener, progressRequestListener);
        return call;

    }

    /**
     * Retrieve accessible Source Repositories in the specified folder
     * 
     * @param folderPath Folder's ID in which artifacts will be created (required)
     * @return List&lt;SourceRepository&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public List<SourceRepository> getSourceRepositoriesInFolder(String folderPath) throws ApiException {
        ApiResponse<List<SourceRepository>> resp = getSourceRepositoriesInFolderWithHttpInfo(folderPath);
        return resp.getData();
    }

    /**
     * Retrieve accessible Source Repositories in the specified folder
     * 
     * @param folderPath Folder's ID in which artifacts will be created (required)
     * @return ApiResponse&lt;List&lt;SourceRepository&gt;&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<List<SourceRepository>> getSourceRepositoriesInFolderWithHttpInfo(String folderPath) throws ApiException {
        com.squareup.okhttp.Call call = getSourceRepositoriesInFolderValidateBeforeCall(folderPath, null, null);
        Type localVarReturnType = new TypeToken<List<SourceRepository>>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * Retrieve accessible Source Repositories in the specified folder (asynchronously)
     * 
     * @param folderPath Folder's ID in which artifacts will be created (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call getSourceRepositoriesInFolderAsync(String folderPath, final ApiCallback<List<SourceRepository>> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = (bytesRead, contentLength, done) -> callback.onDownloadProgress(bytesRead, contentLength, done);
            progressRequestListener = (bytesWritten, contentLength, done) -> callback.onUploadProgress(bytesWritten, contentLength, done);
        }

        com.squareup.okhttp.Call call = getSourceRepositoriesInFolderValidateBeforeCall(folderPath, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<List<SourceRepository>>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }
}
